#FROM openjdk:8-jdk-alpine
#VOLUME /tmp
#ADD target/hello-docker-0.0.1-SNAPSHOT.jar hello-docker-app.jar
#ENV JAVA_OPTS=""
#ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /hello-docker-app.jar" ]


#FROM gradle:6.8.2-jdk11 AS GRADLE-BUILD
FROM gradle:6.8.2-jdk11 AS build
COPY . /home/gradle/src
WORKDIR /home/gradle/src

RUN echo " "
#RUN gradle tasks
#RUN gradle build --parallel --console=verbose -x test
RUN gradle build
RUN echo "LISTING FILES: "
RUN ls -l target/
RUN echo "DONE - code build is now completed ! "
RUN echo " "
RUN echo " "
RUN echo " "

### STAGE 2:RUN ###
FROM openjdk:8-jdk-alpine
RUN echo " "
#Copying new jar to /app
RUN mkdir /app
RUN ls -la
#COPY --from=build /home/gradle/src/*.jar hello-docker-app.jar
COPY --from=build /home/gradle/src/target/*.jar /app/hello-docker-app.jar
#COPY --from=GRADLE-BUILD /home/gradle/src/build/libs/*.jar /app.jar
#RUN cp /home/gradle/src/build/libs/deliverycenter.jar /app.jar
###COPY ../../build/libs/*.jar /app.jar

#RUN java -jar /app/deliverycenter.jar

#ENTRYPOINT ["sh", "-c", "java -Duser.timezone=America/Bahia -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]

#ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom","-jar","/app/deliverycenter.jar"]

ENTRYPOINT ["sh", "-c", "java -Duser.timezone=America/Bahia -Djava.security.egd=file:/dev/./urandom -jar /app/hello-docker-app.jar"]

EXPOSE 8080